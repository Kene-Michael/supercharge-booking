package services.Impl;

import dao.BookingDao;
import dao.PropertyDao;
import dao.UserDao;
import dao.impl.BookingDaoImpl;
import dao.impl.PropertyDaoImpl;
import dao.impl.UserDaoImpl;
import models.Booking;
import models.Property;
import models.TimeRange;
import models.User;
import models.enums.AvailabilityStatus;
import models.enums.BookingStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class UserServiceImplTest {

    @InjectMocks
    private UserDao userDao = new UserDaoImpl();

    @InjectMocks
    private BookingDao bookingDao = new BookingDaoImpl();

    @InjectMocks
    private PropertyDao propertyDao = new PropertyDaoImpl();

    private UserServiceImpl userService;
    private User user;
    private User aSecondUser;
    Property property;
    Booking booking;
    BigDecimal price;


    @BeforeEach
    void setUp(){
        userService = new UserServiceImpl(userDao,bookingDao, propertyDao);
        user = new User(1L, "kene");
        aSecondUser = new User(2L, "ose");
        booking = new Booking();
        price = BigDecimal.valueOf(500L);
        booking.setBookingStatus(BookingStatus.ACTIVE);
        property =
                new Property(
                        1L,
                        new TimeRange(LocalDateTime.now(),
                                LocalDateTime.now().plusDays(2)),
                        AvailabilityStatus.AVAILABLE,
                        BigDecimal.valueOf(500L));
    }

    @Test
    void makeBooking() {
        Property aProperty = new Property();
        aProperty.setAvailability(AvailabilityStatus.AVAILABLE);

        userService.makeBooking(property, user, property.getTimeRange());
        assertEquals(property.getAvailability(), AvailabilityStatus.NOT_AVAILABLE);
        assertEquals(bookingDao.findAll().size(), 1);
    }

    @org.junit.jupiter.api.Test
    void cancelBooking() {
        Booking aBooking = userService.makeBooking(property, user, property.getTimeRange());
        Booking cancelledBooking = userService.cancelBooking(aBooking, user);
        assertEquals(cancelledBooking.getBookingStatus(), BookingStatus.CANCELLED);
    }

    @org.junit.jupiter.api.Test
    void getUserBookings() {
        userService.makeBooking(property, user, property.getTimeRange());
        assertEquals(userService.getUserBookings(user).size(), 1);
        assertEquals(userService.getUserBookings(aSecondUser).size(), 0);
    }

    @org.junit.jupiter.api.Test
    void listAvailableRooms() {
        propertyDao.save(property);
        List<Property> propertyList = userService.listAvailableRooms(AvailabilityStatus.AVAILABLE, price, property.getTimeRange());
        assertEquals(propertyList.size(), 1);
    }

    @org.junit.jupiter.api.Test
    void doesNotlistAvailableRoomsWhenStatusIsUnavailable() {
        propertyDao.save(property);
        List<Property> propertyList = userService.listAvailableRooms(AvailabilityStatus.NOT_AVAILABLE, price, property.getTimeRange());
        assertEquals(propertyList.size(), 0);
    }

    @org.junit.jupiter.api.Test
    void doesNotlistAvailableRoomsWhenPropertyTimeRangeIsOff() {
        propertyDao.save(property);
        TimeRange timeRange = new TimeRange(LocalDateTime.now().minusDays(2L), LocalDateTime.now().minusDays(1L));
        List<Property> propertyList = userService.listAvailableRooms(AvailabilityStatus.AVAILABLE, price, timeRange);
        assertEquals(propertyList.size(), 0);
    }
}