package models;

import models.enums.BookingStatus;

public class Booking {
    private Long id;
    private Property property;
    private User user;
    private TimeRange timeRange;
    private BookingStatus bookingStatus;

    public Booking() {
    }

    public Booking(Long id, Property property, User user, TimeRange timeRange, BookingStatus bookingStatus) {
        this.id = id;
        this.property = property;
        this.user = user;
        this.timeRange = timeRange;
        this.bookingStatus = bookingStatus;
    }

    public BookingStatus getBookingStatus() {
        return bookingStatus;
    }

    public void setBookingStatus(BookingStatus bookingStatus) {
        this.bookingStatus = bookingStatus;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Property getProperty() {
        return property;
    }

    public void setProperty(Property property) {
        this.property = property;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public TimeRange getTimeRange() {
        return timeRange;
    }

    public void setTimeRange(TimeRange timeRange) {
        this.timeRange = timeRange;
    }

    @Override
    public String toString() {
        return "Booking: " +
                "id=" + id +
                ", property=" + property +
                ", user=" + user +
                ", timeRange=" + timeRange +
                ", bookingStatus=" + bookingStatus +
                '}';
    }
}
