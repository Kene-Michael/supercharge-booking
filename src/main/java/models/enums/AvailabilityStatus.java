package models.enums;

public enum AvailabilityStatus {
    NOT_AVAILABLE, AVAILABLE
}
