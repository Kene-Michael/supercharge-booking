package models.enums;

public enum BookingStatus {
    CANCELLED, ACTIVE, EXPIRED
}
