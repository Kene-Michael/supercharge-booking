package models;

import models.enums.AvailabilityStatus;

import java.math.BigDecimal;

public class Property {
    private Long id;
    private String location = "Budapest";
    private TimeRange timeRange;
    private AvailabilityStatus availability;
    private BigDecimal price;

    public Property() {
    }

    public Property(Long id, TimeRange timeRange, AvailabilityStatus availability, BigDecimal price) {
        this.id = id;
        this.timeRange = timeRange;
        this.availability = availability;
        this.price = price;
    }

    public AvailabilityStatus getAvailability() {
        return availability;
    }

    public void setAvailability(AvailabilityStatus availability) {
        this.availability = availability;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public TimeRange getTimeRange() {
        return timeRange;
    }

    public void setTimeRange(TimeRange timeRange) {
        this.timeRange = timeRange;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
