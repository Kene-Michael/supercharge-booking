package services;

import models.Booking;
import models.Property;
import models.TimeRange;
import models.User;
import models.enums.AvailabilityStatus;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

public interface UserService {

    Booking makeBooking(Property property, User user, TimeRange timeRange);
    Booking cancelBooking(Booking booking, User user);
    List<Booking> getUserBookings(User user);
    void printBookingHistory(User user);
    List<Property> listAvailableRooms(AvailabilityStatus status, BigDecimal price, TimeRange timeRange);
}
