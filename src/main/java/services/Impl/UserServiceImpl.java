package services.Impl;

import dao.BookingDao;
import dao.PropertyDao;
import dao.UserDao;
import models.Booking;
import models.Property;
import models.TimeRange;
import models.User;
import models.enums.AvailabilityStatus;
import models.enums.BookingStatus;
import services.UserService;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

public class UserServiceImpl implements UserService {
    private UserDao userDao;
    private BookingDao bookingDao;
    private PropertyDao propertyDao;

    public UserServiceImpl(UserDao userDao, BookingDao bookingDao, PropertyDao propertyDao) {
        this.userDao = userDao;
        this.bookingDao = bookingDao;
        this.propertyDao = propertyDao;
    }

    @Override
    public Booking makeBooking(Property property, User user, TimeRange timeRange) {
        Booking booking = new Booking();
        property.setAvailability(AvailabilityStatus.NOT_AVAILABLE);
        propertyDao.save(property);

        booking.setProperty(property);
        booking.setUser(user);
        booking.setTimeRange(timeRange);
        booking.setBookingStatus(BookingStatus.ACTIVE);

        return bookingDao.save(booking);
    }

    @Override
    public Booking cancelBooking(Booking booking, User user) {
        Booking returnedBooking = bookingDao
                .findByUserIdAndBookingId(user.getId(), booking.getId());
        returnedBooking.setBookingStatus(BookingStatus.CANCELLED);
        return bookingDao.save(booking);
    }

    @Override
    public List<Booking> getUserBookings(User user) {
        return bookingDao.findByUserId(user.getId());
    }

    @Override
    public void printBookingHistory(User user) {
        List<Booking> bookings = getUserBookings(user);
        bookings.forEach(System.out::println);
    }

    @Override
    public List<Property> listAvailableRooms(AvailabilityStatus status, BigDecimal price, TimeRange timeRange) {
        return propertyDao.findPropertiesByAvailabilityAndPriceAndDate(
                status,
                price, timeRange);
    }
}
