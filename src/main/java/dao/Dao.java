package dao;

import java.util.List;

public interface Dao<T, ID> {
    T save(T t);
    T findById(ID id);
    List<T> findAll ();
    void delete(ID id);
}
