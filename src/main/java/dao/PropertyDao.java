package dao;

import models.Property;
import models.TimeRange;
import models.enums.AvailabilityStatus;

import java.math.BigDecimal;
import java.util.List;

public interface PropertyDao extends Dao<Property, Long> {
    List<Property> findPropertiesByAvailabilityAndPriceAndDate(AvailabilityStatus status, BigDecimal price, TimeRange timeRange);
}
