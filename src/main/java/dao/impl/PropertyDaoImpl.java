package dao.impl;

import dao.PropertyDao;
import models.Property;
import models.TimeRange;
import models.enums.AvailabilityStatus;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class PropertyDaoImpl implements PropertyDao {
    private List<Property> propertyList = new ArrayList<>();
    private Long currentId = 0L;

    @Override
    public Property save(Property property) {
        if (property.getId() == null) {
            property.setId(getId());
        } else {
            propertyList = propertyList.stream()
                    .filter(property1 -> !property1.getId().equals(property.getId()))
                    .collect(Collectors.toList());
        }
        propertyList.add(property);
        return property;
    }

    @Override
    public Property findById(Long aLong) {
        return propertyList.stream()
                .filter(property -> property.getId().equals(aLong))
                .findFirst().orElse(null);
    }

    @Override
    public List<Property> findAll() {
        return new ArrayList<>(propertyList);
    }

    @Override
    public void delete(Long aLong) {
        propertyList = propertyList.stream()
                .filter(property -> !property.getId().equals(aLong))
                .collect(Collectors.toList());
    }

    @Override
    public List<Property> findPropertiesByAvailabilityAndPriceAndDate(AvailabilityStatus status, BigDecimal price, TimeRange timeRange) {
        return propertyList.stream().filter(property -> (property.getAvailability().equals(status) && property.getPrice().doubleValue() == price.doubleValue() && (
                (property.getTimeRange().getStart().isEqual(timeRange.getStart()) || property.getTimeRange().getStart().isBefore(timeRange.getStart()))
                && (property.getTimeRange().getEnd().isEqual(timeRange.getEnd()) || property.getTimeRange().getEnd().isAfter(timeRange.getEnd()))
                ))).collect(Collectors.toList());
    }

    private long getId() {
        return currentId++;
    }
}
