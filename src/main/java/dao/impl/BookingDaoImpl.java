package dao.impl;

import dao.BookingDao;
import models.Booking;
import models.Booking;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class BookingDaoImpl implements BookingDao {
    private List<Booking> bookingList = new ArrayList<>();
    private Long currentId = 0L;

    @Override
    public Booking save(Booking booking) {
        if (booking.getId() == null) {
            booking.setId(getId());
        } else {
            bookingList = bookingList
                    .stream()
                    .filter(booking1 -> !booking1.getId().equals(booking.getId()))
                    .collect(Collectors.toList());
        }
        bookingList.add(booking);
        return booking;
    }

    @Override
    public Booking findById(Long aLong) {
        return bookingList.stream()
                .filter(booking -> booking.getId().equals(aLong))
                .findFirst().orElse(null);
    }

    @Override
    public List<Booking> findAll() {
        return new ArrayList<>(bookingList);
    }

    @Override
    public void delete(Long aLong) {
        bookingList = bookingList.stream()
                .filter(booking -> !booking.getId().equals(aLong))
                .collect(Collectors.toList());
    }

    @Override
    public List<Booking> findByUserId(Long id) {
        return bookingList.stream()
                .filter(booking -> booking.getUser().getId().equals(id))
                .collect(Collectors.toList());
    }

    @Override
    public Booking findByUserIdAndBookingId(Long userId, Long bookingId) {
        return bookingList.stream()
                .filter(booking -> booking.getUser().getId().equals(userId) && booking.getId().equals(bookingId))
                .findFirst().orElse(null);
    }

    private long getId() {
        return currentId++;
    }
}
