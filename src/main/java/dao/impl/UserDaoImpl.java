package dao.impl;

import dao.UserDao;
import models.User;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class UserDaoImpl implements UserDao {
    List<User> userList = new ArrayList<>();
    private Long currentId = 0L;

    @Override
    public User save(User user) {
        user.setId(getId());
        userList.add(user);
        return user;
    }

    @Override
    public User findById(Long aLong) {
        return userList.stream()
                .filter(user -> user.getId().equals(aLong))
                .findFirst().orElse(null);
    }

    @Override
    public List<User> findAll() {
        return new ArrayList<>(userList);
    }

    @Override
    public void delete(Long aLong) {
        userList = userList.stream()
                .filter(user -> !user.getId().equals(aLong))
                .collect(Collectors.toList());
    }

    private long getId() {
        return currentId++;
    }
}
