package dao;

import models.Booking;

import java.util.List;

public interface BookingDao extends Dao<Booking, Long> {
    List<Booking> findByUserId (Long id);

    Booking findByUserIdAndBookingId(Long userId, Long bookingId);
}
